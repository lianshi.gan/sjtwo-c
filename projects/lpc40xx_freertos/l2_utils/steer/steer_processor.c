#include "steer_processor.h"
#include "steering.h"

#define STEER_THRESHOLD_CM 50

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm) {
  // Both sensor below the Threshold
  if (left_sensor_cm < STEER_THRESHOLD_CM && right_sensor_cm < STEER_THRESHOLD_CM) {
    if (left_sensor_cm < right_sensor_cm) {
      steer_right();
    } else if (right_sensor_cm < left_sensor_cm) {
      steer_left();
    }

  }
  // Only left is below the threshold
  else if (left_sensor_cm < STEER_THRESHOLD_CM) {
    steer_left();
  }
  // Only right is below the threshold
  else if (right_sensor_cm < STEER_THRESHOLD_CM) {
    steer_right();
  }
}