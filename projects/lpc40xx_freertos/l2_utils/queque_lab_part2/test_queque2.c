#include "unity.h"

// header file that need to be tested
#include "queque2.h"

static queue_s queue;
static uint8_t queue_memory[100];

void test_init(void) {
  // Initialize the queue
  queue_init(&queue, queue_memory, sizeof(queue_memory));
  TEST_ASSERT_NOT_NULL(queue.static_memory_for_queue);
  TEST_ASSERT_EQUAL_PTR(queue_memory, queue.static_memory_for_queue);
  TEST_ASSERT_EQUAL(sizeof(queue_memory), queue.static_memory_size_in_bytes);
  // Test that count is 0
  TEST_ASSERT_EQUAL(0, queue.head);

  // Test that head is 0
  TEST_ASSERT_EQUAL(0, queue.tail);

  // Test that tail is 0
  TEST_ASSERT_EQUAL(0, queue.count);
}

void test_comprehensive_2(void) {

  queue_init(&queue, queue_memory, sizeof(queue_memory));

  const size_t max_queue_size = sizeof(queue_memory); // Use the actual size of queue_memory

  // Test pushing items until the queue is full
  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue_push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue_get_item_count(&queue));
  }

  // Queue should be full now, additional push should fail
  TEST_ASSERT_FALSE(queue_push(&queue, 123));
  TEST_ASSERT_EQUAL(max_queue_size, queue_get_item_count(&queue));

  // Test popping items and verify FIFO order
  for (size_t item = 0; item < max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue_pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  // Queue should be empty now, additional pop should fail
  uint8_t popped_value = 0;
  TEST_ASSERT_FALSE(queue_pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(0, queue_get_item_count(&queue));

  // Test wrap-around case
  const uint8_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue_push(&queue, pushed_value));
  TEST_ASSERT_TRUE(queue_pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);
  TEST_ASSERT_EQUAL(0, queue_get_item_count(&queue));
}
