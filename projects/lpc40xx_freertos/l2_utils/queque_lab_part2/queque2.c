#include "queque2.h"

void queue_init(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes) {
  queue->static_memory_for_queue = static_memory_for_queue;
  queue->static_memory_size_in_bytes = static_memory_size_in_bytes;
  queue->head = 0;
  queue->tail = 0;
  queue->count = 0;
}

bool queue_push(queue_s *queue, uint8_t push_value) {
  if (queue->count >= queue->static_memory_size_in_bytes) {
    return false;
  }

  size_t index = (queue->tail) % queue->static_memory_size_in_bytes;
  queue->static_memory_for_queue[index] = push_value;

  // Update tail and count
  queue->tail = (queue->tail + 1) % queue->static_memory_size_in_bytes;
  queue->count++;

  return true;
}

bool queue_pop(queue_s *queue, uint8_t *pop_value_ptr) {
  if (queue->count == 0) {
    return false;
  }

  size_t index = (queue->head) % queue->static_memory_size_in_bytes;
  *pop_value_ptr = queue->static_memory_for_queue[index];

  // Update tail and count
  queue->head = (queue->head + 1) % queue->static_memory_size_in_bytes;
  queue->count--;

  return true;
}

size_t queue_get_item_count(const queue_s *queue) { return queue->count; }