
#include "gpio.h"

// Initializes
void switch_led_logic__initialize(void);

// Reads the switch state and set LED
void switch_led_logic__run_once(void);
