#include "unity.h"

#include "Mockmessage.h"

#include "message_processor.h"

void setUp(void) { Mockmessage_Init(); }

void tearDown(void) {
  Mockmessage_Verify();
  Mockmessage_Destroy();
}

static bool message__read_stub(message_s *message_to_read, int call_count) {
  bool message_was_read = false;

  if (call_count >= 2) {
    message_was_read = false;
  } else {
    message_was_read = true;
  }

  if (call_count == 0) {
    message_to_read->data[0] = 'x';
  }
  if (call_count == 1) {
    message_to_read->data[0] = '$';
  }

  return message_was_read;
}

// This only tests if we process at most 3 messages
void test_process_messages_with_stubWithCallback(void) {
  message__read_StubWithCallback(message__read_stub);

  // Call the function under test
  bool symbol_found = message_processor();

  // Since the scenario setup involves finding the symbol '$' in the second message,
  // the expectation is that symbol_found should be true.
  TEST_ASSERT_TRUE(symbol_found);
}

// Add more tests if necessary